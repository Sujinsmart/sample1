import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class FormserviceService {
  private URL = 'http://localhost:8080/api/rest';

  constructor(private http: HttpClient) {}

  postnew(data: any): Observable<object> {
    // return this.http.post(this.URL, data);
    return this.http.post(this.URL + '/form', data);
  }

  getDetails(): Observable<[]> {
    return this.http.get<[]>(this.URL + '/form');
  }

  delete(id: number): Observable<Object> {
    return this.http.delete(this.URL + '/form/' + id);
  }
}
