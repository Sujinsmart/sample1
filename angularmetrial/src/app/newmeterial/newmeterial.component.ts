import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { FormserviceService } from '../formservice.service';
import { MeterialModule } from '../meterial/meterial.module';

@Component({
  selector: 'app-newmeterial',
  templateUrl: './newmeterial.component.html',
  styleUrls: ['./newmeterial.component.css'],
})
export class NewmeterialComponent implements OnInit {
  constructor(
    private matmod: MeterialModule,
    private service: FormserviceService,
    private route: Router
  ) {}
  check!: false;

  userprofileForm!: FormGroup;

  bioSection = new FormGroup({
    firstname: new FormControl(),
    lastname: new FormControl(),
    age: new FormControl(),
  });

  callingFunction() {
    this.service.postnew(this.bioSection.value).subscribe((data) => {
      alert('details added');
      console.log(this.bioSection.value);
    });
  }

  ngOnInit(): void {}

  // onchange(event: any) {
  //   console.log(event);
  // }
  getdetails() {
    console.log(this.check);
    this.route.navigate(['/getform']);
  }
}
