import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewmeterialComponent } from './newmeterial.component';

describe('NewmeterialComponent', () => {
  let component: NewmeterialComponent;
  let fixture: ComponentFixture<NewmeterialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewmeterialComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NewmeterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
