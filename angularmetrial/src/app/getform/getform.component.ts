import { Component, OnInit } from '@angular/core';
import { FormInterface } from '../form-interface';

import { FormserviceService } from '../formservice.service';

@Component({
  selector: 'app-getform',
  templateUrl: './getform.component.html',
  styleUrls: ['./getform.component.css'],
})
export class GetformComponent implements OnInit {
  form2: FormInterface[] = [];
  dataSource: any;
  displayedColumns: string[] = ['id', 'firstname', 'lastname', 'age', 'button'];
  constructor(private service: FormserviceService) {}

  ngOnInit(): void {
    this.getAll();
  }

  getAll() {
    this.service.getDetails().subscribe((data) => {
      this.form2 = data;
      this.dataSource = this.form2;
    });
  }

  onDelete(id: number) {
    this.service.delete(id).subscribe((pass) => {
      this.ngOnInit();
    });
  }
}
