import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GetformComponent } from './getform.component';

describe('GetformComponent', () => {
  let component: GetformComponent;
  let fixture: ComponentFixture<GetformComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GetformComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GetformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
