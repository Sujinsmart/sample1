import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NewmeterialComponent } from './newmeterial/newmeterial.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'angularmetrial';

  firstFormGroup = this._formBuilder.group({
    firstCtrl: ['', Validators.required],
  });
  secondFormGroup = this._formBuilder.group({
    secondCtrl: ['', Validators.required],
  });
  constructor(
    private dialog: MatDialog,
    private router: Router,
    private toastr: ToastrService,
    private _formBuilder: FormBuilder
  ) {}

  showtoastr() {
    this.toastr.error('appointment cancel', 'Hello', {
      timeOut: 1000,
    });
  }

  dialog1() {
    this.dialog.open(NewmeterialComponent);
  }
  userlist() {
    this.router.navigate(['getform']);
  }
}
