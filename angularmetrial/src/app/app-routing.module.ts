import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GetformComponent } from './getform/getform.component';
import { NewmeterialComponent } from './newmeterial/newmeterial.component';

const routes: Routes = [
  { path: 'new', component: NewmeterialComponent },
  { path: 'getform', component: GetformComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
