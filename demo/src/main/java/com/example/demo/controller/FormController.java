package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.example.demo.entity.Form;
import com.example.demo.service.FormService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/rest")
public class FormController {

	@Autowired
	FormService formserice;
	
	@PostMapping("/form")
	public Form appointmentData(@RequestBody Form data) {
		return this.formserice.saveform(data);
}
	@GetMapping("/form")
	public List<Form> all(){
	 return this.formserice.alldata();
	}
	
	@DeleteMapping("/form/{id}")
	public  void delete(@PathVariable Long id) {
		this.formserice.deleteone(id);
	}
}