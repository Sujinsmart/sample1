package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.example.demo.entity.Form;
import com.example.demo.repository.FormRepositry;

@Service
public class FormService {
	
	@Autowired
	FormRepositry formRepository;

	public Form saveform(Form data) {
		return this.formRepository.save(data);
	}
	
	

	public List<Form> alldata() {
		
		return this.formRepository.findAll();
	}



	public void deleteone(Long id) {
		this.formRepository.deleteById(id);
		
	}
}
